# ![webhook-logo](assets/.README/webhook.png) Discord Webhook Tester
an easy way to test discord Webhook's
## Installation
**[Python 3.6.0](https://www.python.org/downloads/release/python-360/) is required!**

install [git](https://git-scm.com/downloads) and do ``git clone https://gitlab.com/oculus-dev/python/dwt.git``\
or download the [zip](https://gitlab.com/oculus-dev/python/dwt/-/archive/master/dwt-master.zip) and unzip it with [7-Zip](https://www.7-zip.org/).

```bash
# use python3 or python depends on the installation

# install pip
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py

# download the requirements
python -m pip install -r requirements.txt
```
## Usage

```bash
# use python3 or python depends on the installation
python webhook.py -h

usage: webhook.py [-h] [-g GUI] [-u URL] [-j JSON] [-v] [-n]

Youtube Downloader: Download videos from Youtube

optional arguments:
  -h, --help            show this help message and exit
  -g GUI, --gui GUI     0 = Gui, 1 = Terminal GUI, 2 = NO GUI
  -u URL, --url URL     video url
  -j JSON, --json JSON  json path
  -v, --verbose         debug info
  -n, --nocolor         no color for terminals that dont support color
```
