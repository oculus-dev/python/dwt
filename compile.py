import os
import tempfile
import shutil

temp = tempfile.TemporaryDirectory()
image = os.path.join(os.getcwd(), "assets", "webhook.ico")
py_name = 'webhook'
py_path = os.path.join(os.getcwd(), py_name + '.py')

if not (os.path.exists(image) and os.path.isfile(image)):
    print("image not found")
    exit()

if not (os.path.exists(py_path) and os.path.isfile(py_path)):
    print("py-file not found")
    exit()

new_py = os.path.join(temp.name, py_name + '.py')
shutil.copyfile(py_path, new_py)

os.system('pyinstaller --onefile '
          '--distpath "{}" --specpath "{}" --workpath "{}" --runtime-tmpdir "{}" --icon "{}" "{}"'.format(
    os.getcwd(),
    temp.name,
    temp.name,
    temp.name,
    image,
    new_py
))

temp.cleanup()
