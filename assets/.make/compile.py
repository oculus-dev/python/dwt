#!/usr/bin/python3
# coding=utf-8

import os
if not (os.path.exists(".compile") and os.path.isdir(".compile")):
    os.mkdir(".compile")

out_path = os.path.join(os.getcwd(), ".compile")
in_path = os.path.join(os.getcwd(), "..")

for file in os.listdir(in_path):
    if file.endswith('.ui'):
        os.popen('pyuic5 -x "' + os.path.join(in_path, file) + '" -o "' + os.path.join(out_path, file.replace('.ui', '.py')) + '"')
