import os
from zipfile import ZipFile
import base64

if not (os.path.exists(".encode") and os.path.isdir(".encode")):
    os.mkdir(".encode")

out_path = os.path.join(os.getcwd(), ".encode")
in_path = os.path.join(os.getcwd(), "..")
zip_path = os.path.join(out_path, "encode.zip")
encode_path = os.path.join(out_path, "encode.txt")

with ZipFile(zip_path, 'w') as ziped:
    os.chdir(in_path)
    for file in os.listdir(in_path):
        if not file.startswith("."):
            ziped.write(file)

f = open(zip_path, "rb")
encode = base64.b64encode(f.read())
f = open(encode_path, "wb")
f.write(encode)
f.close()
